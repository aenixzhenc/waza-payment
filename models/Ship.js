//Require Mongoose
var mongoose = require("mongoose");

var schema = mongoose.Schema;

//Định nghĩa một schema
//Schema
var ShipSchema = schema

var ShipSchema = new schema(
  {
    UserId:String,
    BillId:String,
    Distance:Number,
    Handle:{type:Boolean, default:false}
  },
  { timestamps: true }
);

mongoose.model('Ship', ShipSchema);
