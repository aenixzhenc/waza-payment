//Require Mongoose
var mongoose = require("mongoose");

var schema = mongoose.Schema;

//Định nghĩa một schema
//Schema
var Charge = schema;

var Charge = new schema(
  {
    param_id: {
      type: String,
      require: true
    },
    param_value: {
      type: Number,
      default: 0,
      min: 0,
      max: 100
    },
    from_date:{
      type:Date
    },
    to_date:{
      type:Date
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Charge", Charge);