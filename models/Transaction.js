//Require Mongoose
var mongoose = require("mongoose");

var schema = mongoose.Schema;

//Định nghĩa một schema
//Schema
var TransactionSchema = schema;

var TransactionSchema = new schema(
  {
    UserId: String,
    Amount: Number ,
    Currency: String,
    Type: String,
    
    Description: String
  },
  { timestamps: true }
);

mongoose.model("Transaction", TransactionSchema);
