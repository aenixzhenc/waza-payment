//Require Mongoose
var mongoose = require("mongoose");

var schema = mongoose.Schema;

//Định nghĩa một schema
//Schema
var ShipFee = schema;

var ShipFee = new schema(
  {
    param_id: {
      type: String,
      require: true
    },
    param_value: {
      type: Number,
      default: 0,
      min: 0
    },
    from_date:{
      type:Date
    },
    to_date:{
      type:Date
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("ShipFee", ShipFee);
