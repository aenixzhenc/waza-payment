//Require Mongoose
var mongoose = require("mongoose");

var schema = mongoose.Schema;

//Định nghĩa một schema
//Schema
var Rush = schema;

var Rush = new schema(
  {
    name: String,
    amount: {
      type: Number,
      default: 0,
      min: 0
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Rush", Rush);
