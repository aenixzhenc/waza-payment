//Require Mongoose
var mongoose = require("mongoose");

var schema = mongoose.Schema;

//Định nghĩa một schema
//Schema
var BillSchema = schema;

var BillSchema = new schema(
  {
    UserId:String,
    BillId:String,
    Total:Number,
    Handle:{type:Boolean, default:false}
  },
  { timestamps: true }
);

//
mongoose.model('Bill', BillSchema);
