//Require Mongoose
var mongoose = require('mongoose');

var schema = mongoose.Schema;

//Định nghĩa một schema
//Schema
var Wallet = schema;

var Wallet = new schema(
  {
    user_id: String,
    wallet_type: String,
    total: {
      type: Number,
      default: 0,
      min: 0
      }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Wallet", Wallet);
