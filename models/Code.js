//Require Mongoose
var mongoose = require("mongoose");

var schema = mongoose.Schema;

//Định nghĩa một schema
//Schema
var CodeSchema = schema;

var CodeSchema = new schema(
  {
    CodeId: { type: String, minlength: 16, maxlength: 16 },
    Amount: Number,
    Handle:{type:Boolean, default:false}
  },
  { timestamps: true }
);

mongoose.model("Code", CodeSchema);
