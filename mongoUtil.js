//lets require/import the mongodb native drivers.
var mongodb = require("mongodb");

//We need to work with "MongoClient" interface in order to connect to a mongodb server.
var MongoClient = mongodb.MongoClient;

// Connection URL. This is where your mongodb server is running.
var url = "mongodb://localhost:27017";

//create var database to reuse
var _db;

module.exports = {
  connectToServer: function(callback) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
      if (err) {
        console.log("Unable to connect to the mongoDB server. Error:", err);
      } else {
        //HURRAY!! We are connected. :)
        console.log("Connection established to", url);
        _db = client.db("waza-payment");
      }
      return callback(err);
    });
  },

  getDb: function(err) {
    return _db;
  }
};

// MongoClient.connect(url);

// Use connect method to connect to the Server
// MongoClient.connect(url, function(err, db) {
//   if (err) {
//     console.log("Unable to connect to the mongoDB server. Error:", err);
//   } else {
//     //HURRAY!! We are connected. :)
//     console.log("Connection established to", url);

//     // Get the documents collection
//     //var collection = db.collection("");

//     //Close connection
//     db.close();
//   }
// });
