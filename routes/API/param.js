//Modules
var express = require('express');
var db = require('../../models');

router = express.Router();

router.post('/getCharge', function(req, res, next) {
    db.Charge.findOne({param_id: req.body.param_id},  function (err, charge) {
        if (err) return handleError(err);
        res.send(JSON.stringify(charge.param_value));
    })
    .catch(function(e) {
        res.status(500).send(JSON.stringify(e));
    });
});

router.post('/getDeposit', function(req, res, next) {
    db.Deposit.findOne({param_id: req.body.param_id},  function (err, charge) {
        if (err) return handleError(err);
        res.send(JSON.stringify(charge.param_value));
    })
    .catch(function(e) {
        res.status(500).send(JSON.stringify(e));
    });
});

router.post('/getShipfee', function(req, res, next) {
    db.ShipFee.findOne({param_id: req.body.param_id},  function (err, charge) {
        if (err) return handleError(err);
        res.send(JSON.stringify(charge.param_value));
    })
    .catch(function(e) {
        res.status(500).send(JSON.stringify(e));
    });
});

module.exports = router;