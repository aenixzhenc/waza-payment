var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");

/* GET bill listing. */
// router.get("/", function(req, res, next) {
//   res.render("billDetail");
// });

router.post("/fee", function(req, res) {
  var user_id = req.body.user_id;
  var distance = req.body.distance;

  var amount = 15000 * distance;

  res.json({
    amount: amount
  });
});

module.exports = router;