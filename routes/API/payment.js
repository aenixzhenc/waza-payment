//Modules
var express = require("express");
var db = require("../../models");
var fetch = require("node-fetch");

//Usage
var router = express.Router();

//
router.get("/shipfee", function(req, res, next) {
  // db.ShipFee({
  //     name: 'bike_fee',
  //     amount: 15
  // }).save();

  db.ShipFee.count({}, function(err, c) {
    db.ShipFee.find()
      .sort({ name: "desc" })
      .then(function(shipfees) {
        if (err) {
          throw true;
        }
        res.render("shipfee", { shipfeelist: shipfees });
      })
      .catch(function(e) {
        res.status(500).send(JSON.stringify(e));
      });
  });
});

router.post("/shipfee", function(req, res, next) {});
router.post("/shipfee", function(req, res, next) {});
router.post("/shipfee", function(req, res, next) {});

router.get("/deposit", function(req, res, next) {
  db.Deposit.count({}, function(err, c) {
    db.Deposit.find()
      .sort({ name: "desc" })
      .then(function(deposits) {
        if (err) {
          throw true;
        }
        res.render("deposits", { depositlist: deposits });
      })
      .catch(function(e) {
        res.status(500).send(JSON.stringify(e));
      });
  });
});

// router.get("/charge", function(req, res, next) {
//   db.Charge.count({}, function(err, c) {
//     db.Charge.find()
//       .sort({ name: "desc" })
//       .then(function(charges) {
//         if (err) {
//           throw true;
//         }
//         res.render("paramlogin", { chargelist: charges });
//         // res.render("charge", { chargelist: charges });
//       })
//       .catch(function(e) {
//         res.status(500).send(JSON.stringify(e));
//       });
//   });
// });

router.get("/charge/add", function(req, res, next) {
  var charge = new db.Charge(req.body);
});
router.post("/charge/add", function(req, res, next) {
  var charge = new db.Charge(req.body);
});
router.post("/charge/update", function(req, res, next) {});
router.post("/charge/delete", function(req, res, next) {});

// router.get('/param',function(req, res, next) {
//   db.payment-param.count({}, function(err, c) {
//     db.payment-param.find()
//       .sort({ param_id: "desc" })
//       .then(function(paramlist) {
//         if (err) {
//           throw true;
//         }
//         res.render('param', {
//           parameter: "test",
//           list: paramlist
//           });
//       })
//       .catch(function(e) {
//         res.status(500).send(JSON.stringify(e));
//       });
//   });
// });

// router.get('/param',function(req, res, next) {
//   db.payment-param.count({}, function(err, c) {
//     db.payment-param.find()
//       .sort({ param_id: "desc" })
//       .then(function(paramlist) {
//         if (err) {
//           throw true;
//         }
//         res.render('param', {
//           parameter: "test",
//           list: paramlist
//           });
//       })
//       .catch(function(e) {
//         res.status(500).send(JSON.stringify(e));
//       });
//   });
// });

router.get("/param", function(req, res, next) {
  var object = {};
  var key = "response";
  object[key] = [];
  fetch("https://waza-payment.herokuapp.com/get_all_param")
    .then(res => res.json())
    // .then(json => console.log(json))
    .then(json => res.render('param',{
      parameter: 'value',
      list: json.response
    }));
    // .then(
    //   json =>
    //     function() {
    //       json.response.forEach(element => {
    //         if (element.param_id.includes("fee")) {
    //           object[key].push(element);
    //           // json.push(element);
    //         }
    //       });
    //       console.log(object[key]);
    //       res.render("param", {
    //         parameter: "value",
    //         list: JSON.stringify(object[key])
    //       });
    //     }
    // );
});

module.exports = router;
