var router = require('express').Router();

//router.use('/', require('./users'));
router.use('/payment', require('./payment'));
router.use('/wallet', require('./wallet'));
router.use('/param', require('./param'));
// router.use('/shipping', require('./shipping'));
// router.use('/pp', require('./payment_parameter'));

router.use(function(err, req, res, next){
  if(err.name === 'ValidationError'){
    return res.status(422).json({
      errors: Object.keys(err.errors).reduce(function(errors, key){
        errors[key] = err.errors[key].message;

        return errors;
      }, {})
    });
  }

  return next(err);
});

module.exports = router;