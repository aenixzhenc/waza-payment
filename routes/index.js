var router = require('express').Router();
var mongo = require('mongodb');

router.use('/api', require('./API'));
router.use('/param', require('./param'))
// router.use('/charge',require('./charge'))

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Waza-Payment' });
});

router.get('/login', function(req, res, next){
  res.render('login'); 
});

module.exports = router;
