var router = require('express').Router();
var db = require("../models");

router.get('/charge', function(req, res, next) {
  db.ShipFee.count({}, function(err, c) {
    db.ShipFee.find()
      .sort({ name: "desc" })
      .then(function(charges) {
        if (err) {
          throw true;
        }
        // res.render("param", { list: charges });
        res.render('chargeAdd');
      })
      .catch(function(e) {
        res.render("param", { list: {} });
      });
  });
});



router.get('/deposit', function(req, res, next) {
  db.ShipFee.count({}, function(err, c) {
    db.ShipFee.find()
      .sort({ name: "desc" })
      .then(function(deposits) {
        if (err) {
          throw true;
        }
        res.render("param", { list: deposits });
      })
      .catch(function(e) {
        res.render("param", { list: {} });
      });
  });
});

router.get('/shipfee', function(req, res, next) {
  db.ShipFee.count({}, function(err, c) {
    db.ShipFee.find()
      .sort({ name: "desc" })
      .then(function(shipfees) {
        if (err) {
          throw true;
        }
        res.render("param", { list: shipfees });
      })
      .catch(function(e) {
        res.render("param", { list: {} });
      });
  });
});

module.exports = router;
