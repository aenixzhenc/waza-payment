var router = require('express').Router();
var db = require('../../models');

router.get('/', function(req, res, next) {
  db.ShipFee.count({}, function(err, c) {
    db.ShipFee.find()
      .sort({ name: 'desc' })
      .then(function(items) {
        if (err) {
          throw true;
        }
        res.render('shipfee', { list: items });
      })
      .catch(function(e) {
        res.status(500).send(JSON.stringify(e));
      });
  });
});

router.post('/', function(req, res, next){
});

router.get('/add', function(req, res, next){
  res.render('shipfee/add'); 
});

router.post('/add', function(req, res, next){
  var paramId = req.body.param_id;
  var paramValue = req.body.param_value;
  var fromDate = req.body.from_date;
  var toDate = req.body.to_date;
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = yyyy + '-' + mm + '-' + dd;
  
  if (paramId == null){
      res.send('param id field must not be empty');
  }

  if (paramValue == null){
      res.send('param value field must not be empty');
  }

  if (paramValue < 1){
      res.send('param value must be greater than or equal 1');
  }

  if (fromDate == null){
      res.send('from date field must not be empty');
  }

  if (toDate == null){
      res.send('to date field must not be empty');
  }

  if (fromDate < today){
      res.send('from Date must be greater than or equal today');
  }

  if (toDate <= fromDate){
      res.send('to Date must be greater than from Date');
  }

  var shipfee = new db.ShipFee(req.body);
  shipfee.save(function (err) {
    if (err) return handleError(err);
    // saved!
  });
        res.redirect('/param/shipfee');
});

router.get('/update', function(req, res, next){
  res.render('charge/add'); 
});


router.post('/update', function(req, res, next){
  const filter = { param_id: req.body.param_id };
  const update = { param_value: req.body.param_value }; 

    db.ShipFee.findOneAndUpdate(
      filter,
      update,
      function(err, doc) {
        console.log(filter);
        console.log(update);
        console.log(doc);

        if (err) return res.send(500, {error: err});
        res.redirect('/param/shipfee');
    })
      .catch(function(e) {
        res.status(500).send(JSON.stringify(e));
      });
});

router.post('/delete', function(req, res, next){
});

module.exports = router;
